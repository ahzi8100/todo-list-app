<?php

require_once('function/helper.php');
require_once('function/koneksi.php');

?>

<!-- Meengambil data dari database -->
<?php

$todoku = mysqli_query($koneksi, "SELECT * FROM todoku");


?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ToDo List App</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= BASE_URL . '/assets/main.css' ?>">
</head>

<body>
    <div class="container">
        <div class="row mb-5">
            <form class="row g-3 d-flex justify-content-center" method="POST" action="<?= BASE_URL . '/process/process_addTodo.php' ?>">
                <h1 class="text-center">Todo List App</h1>
                <div class="col-auto">
                    <label for="inputTodo" class="visually-hidden">Todo</label>
                    <input type="text" name="inputTodo" class="form-control" id="inputTodo" placeholder="Maukkan Todo">
                </div>
                <div class="col-auto">
                    <label for="inputTanggal" class="visually-hidden">Tanggal</label>
                    <input type="date" name="inputTanggal" class="form-control" id="inputTanggal">
                </div>
                <div class="col-auto">
                    <label for="inputTime" class="visually-hidden">Waktu</label>
                    <input type="time" name="inputTime" class="form-control" id="inputTime">
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary mb-3">Tambah Todo</button>
                </div>
            </form>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="list-todo w-50">
                <ul class="list-group list-group-flush">
                    <?php foreach ($todoku as $todo) : ?>
                        <li class="list-group-item d-flex justify-content-between"><?= $todo['todo'] ?>
                            <div class="read-tanggal">
                                <?= $todo['tanggal'] ?>
                            </div>
                            <div class="read-time">
                                <?= $todo['waktu'] ?>
                            </div>
                            <span class="action">
                                <a class="btn btn-danger badge" href="<?= BASE_URL . 'process/process_deleteTodo.php?id=' . $todo['id'] ?>">Done</a>
                            </span>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>

</html>